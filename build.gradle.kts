plugins {
    kotlin("jvm") version ("1.8.0")
    id("com.github.weave-mc.weave") version ("8b70bcc707")

    // scala
    id("org.gradle.scala")
}

group = "com.gitlab.candicey.antismok"
version = "0.1.0"

minecraft.version("1.8.9")

repositories {
    mavenCentral()
    maven("https://jitpack.io")
}

dependencies {
    testImplementation(kotlin("test"))

    // scala
    implementation("org.scala-lang:scala-library:2.13.10")

    compileOnly("com.github.Weave-MC:Weave-Loader:70bd82faa6")
}

tasks.test {
    useJUnitPlatform()
}

kotlin {
    jvmToolchain(8)
}

tasks.jar {
    val wantedJar = listOf<String>()
    configurations["compileClasspath"]
        .filter { wantedJar.find { wantedJarName -> it.name.contains(wantedJarName) } != null }
        .forEach { file: File ->
            from(zipTree(file.absoluteFile)) {
                this.duplicatesStrategy = DuplicatesStrategy.EXCLUDE
            }
        }
}